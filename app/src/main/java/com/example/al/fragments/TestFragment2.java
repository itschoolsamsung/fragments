package com.example.al.fragments;


import android.os.Bundle;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class TestFragment2 extends Fragment {

    TextView textView2;

    public TestFragment2() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_test_fragment2, container, false);
        textView2 = view.findViewById(R.id.textView2);
        view.findViewById(R.id.button2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TestFragment3 fragment3 = (TestFragment3)getChildFragmentManager().findFragmentById(R.id.fragment3);
//                Log.i("_Q_", "onClick: " + Boolean.toString(fragment3 != null));
                fragment3.textView3.setText("zxcvbn");
            }
        });
        return view;
    }

}
