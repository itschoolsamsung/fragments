package com.example.al.fragments;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    TestFragment2 fragment2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        fragment2 = (TestFragment2)getFragmentManager().findFragmentById(R.id.fragment2);
        fragment2 = (TestFragment2)getSupportFragmentManager().findFragmentById(R.id.fragment2);
    }

    public void setTextToFragment2(String text) {
        fragment2.textView2.setText(text);
    }
}
