package com.example.al.fragments;


import android.os.Bundle;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class TestFragment3 extends Fragment {

    TextView textView3;

    public TestFragment3() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_test_fragment3, container, false);
        textView3 = view.findViewById(R.id.textView3);
        Log.i("_Q_", "onCreateView: " + textView3.getText());
        return view;
    }

}
