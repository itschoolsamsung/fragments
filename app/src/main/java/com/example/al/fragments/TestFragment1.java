package com.example.al.fragments;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class TestFragment1 extends Fragment implements View.OnClickListener {

    TextView textView;
    MainActivity activity;

    public TestFragment1() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_test_fragment1, container, false);
        textView = view.findViewById(R.id.textView);
        activity = (MainActivity)getActivity();
        view.findViewById(R.id.button).setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        if (R.id.button == v.getId()) {
            textView.setText("qwerty");
            activity.setTextToFragment2("asdfgh");
        }
    }
}
